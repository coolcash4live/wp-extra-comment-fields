﻿Wp Extra Comment Fields version 2.24

January 2013 
====================

It's been fun making this plugin, We hope that by turning this plugin loose on WordPress others may enjoy and improve it!

Thank you to Tom Wright and Solace for all the work put into this plugin.


Installation
====================

Just upload the folder to your plugin directory, then navigate to your installation admin area and activate the plugin.

Some options appear in settings, and you can configure the extra fields under Comments section.


Description
====================

What this plugin does

The standard WordPress comments form is restricted only to name, URL, email and comments.  With our powerful plugin (we call ECF), you can make your standard WordPress Comment Form extra useful by adding your own, customised fields.


Features
====================

Add extra comment fields to the WordPress comment form.
 - Text Fields
 - Text Area Boxes
 - Dropdown Selects
 - Radios
 - Checkboxes
 - Image upload fields

* Choose where to display these extra comment fields by specific posts/pages ID	
* Select a specific CATEGORY to display in (or display in all)	
* You can specify MANDATORY field or not for any extra comment field 	
* Make PUBLIC or keep them PRIVATE
    
* Your new comment fields are shown in the admin comments section
* If made Public, the fields are not shown until you approve the comment
* Or choose to auto-approve the comments (via WP standard comment option)
* Re-order the fields in admin and easily Edit or Delete the fields
* Display the Extra Comment Fields in the admin email notification that is sent to post author on comment submission
* Simple, easy to use Admin interface
* Fully automated – no manual coding required, just upload, activate & configure!
* WordPress compliant to latest standards
* If desired, you can modify your theme’s CSS to customise how the extra fields display


SUPPORT
====================
Support via the WordPress plugin repository (when available)