<?php
class ECF_Image_Upload extends ECF_Field_Type {
	protected $name = 'image';

	private static final function thumbnail( $url, $h='null',
			$w='null', $zc=3 ) {
		return plugins_url( 'wp-extra-comment-fields/' )
			. "timthumb.php?src=$url&zc=$zc&h=$h&w=$w";
	}

	public function form_field( $ref, $field ) {
		global $ecfdb;
		?>
		<label class='ecf-form-field-title' for="<?php echo $ref ?>">
			<?php echo $ecfdb->html_string( $field->name ); ?>
		</label>
		<input type="file" class='ecf-form-field-input'
			name="<?php echo $ref ?>"
			id="<?php echo $ref ?>"
			onchange='
			document.getElementById("commentform").enctype = "multipart/form-data"' />
		<?php
	}

	public function display_field( $id, $name, $value ) {
		$url = self::thumbnail( $value, 400, 400 );
		$larger = self::thumbnail( $value, 600, 800 );
		return "<figure class='ecf-field ecf-field-$id'>\n"
			. "<div class='ecf-response'><a href='$larger'>"
			. "<img src='$url' class='ecf-image'"
			. "onerror='this.src = \"$value\"' alt='$name' /></a></div>"
			. "\t<figcaption class='ecf-question'>$name</figcaption>"
			. "</figure>\n";
	}

	public function display_plaintext_field( $name, $value ) {
		$url = self::thumbnail( $value, 400, 400 );
		return "$name:\n$url";
	}

	public function get_description() {
		return "Image upload field";
	}
}

new ECF_Image_Upload();
?>