<?php
/* Checkboxes form */
class ECF_Checkboxes extends ECF_Field_Type {
	protected $name = 'checkboxes';

	public function form_field( $ref, $field ) {
		global $ecfdb;
		$name = $ecfdb->html_string( $field->name );
		echo "<div class='ecf-form-field-title'>$name</div>\n";
		$values = array_map( array( &$ecfdb, 'html_string' ),
			preg_split( '/, ?/', $field->values ) );
		echo '<div class=\'ecf-form-field-input\'>';
		foreach ( $values as $i => $value ) {
			echo "<p><label><input type='checkbox' name='${ref}[]' "
				. "value='$value' /> $value</label></p>\n";
		}
		echo '</div>';
		
		// Check at least one box is checked via javascript
		if ( ECF_Option::get_option( 'required' )->get_value( $field ) ) { ?>
			<script type="text/javascript">
			jQuery('form[id^=comment]').submit(function() {
				var fields = jQuery('input[name="<?php echo "${ref}[]" ?>"]')
					.serializeArray();
				var numberOfCheckboxes = <?php echo count( $values ); ?>;
				var name = "<?php echo $name ?>";
				if (fields.length == 0) {
					if (numberOfCheckboxes > 1)
						alert(name + " must have at least one box checked.");
					else
						alert(name + " must be checked.");
					return false;
				}
			});
			</script>
		<?php }
	}

	public function display_field( $id, $name, $values ) {
		$output = "<div class='ecf-field ecf-field-$id'>"
			. "<strong class='ecf-question'>$name:</strong>";
		$output .= "<div class='ecf-response'>"
			. "<ul style='margin-bottom: 0' class='ecf_checked_list'>\n";
		assert(is_array($values));
		foreach ($values as $value) {
			$output .= "<li style='padding-top: 0'>$value\n";
		}
		$output .= "</ul></div></div>\n";
		return $output;
	}

	public function display_plaintext_field( $name, $values ) {
		$output = "$name:\n";
		foreach ($values as $value) {
			$output .= " * $value\n";
		}
		return $output;
	}

	public function get_description() {
		return "Checkbox selection field";
	}
}

new ECF_Checkboxes();
?>
