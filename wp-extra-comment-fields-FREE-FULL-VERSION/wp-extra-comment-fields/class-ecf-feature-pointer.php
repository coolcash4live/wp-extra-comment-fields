<?php
/**
 * Create a WordPress Feature Pointer
 */
class ECF_Feature_Pointer {
	// The name of the feature pointer
	private $name;
	// A CSS selector for the element to which the pointer is attached
	private $selector;
	// The title / heading of the pointer
	private $title;
	// The content of the pointer
	private $body;
	// The edge of the element to which the pointer is attached
	private $edge;
	// The alignment of the feature pointer
	private $align;

	function __construct( $name, $selector, $title, $body, $edge = 'left',
		$align = 'center' ) {
		$this->name = $name;
		$this->selector = $selector;
		$this->title = $title;
		$this->body = $body;
		$this->edge = $edge;
		$this->align = $align;

		add_action( 'admin_enqueue_scripts',
			array( &$this, 'enqueue_scripts_and_styles' ) );
	}

	public function enqueue_scripts_and_styles() {
		if ( ! in_array( $this->name, self::dismissed_pointers() ) ) {
			add_action( 'admin_print_footer_scripts',
				array( &$this, 'script' ) );

			wp_enqueue_script( 'wp-pointer' );
			wp_enqueue_style( 'wp-pointer' );
		}
	}

	public function script() {
		$content = sprintf( '<h3>%s</h3>%s', $this->title, $this->body );
?>
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$('<?php echo $this->selector; ?>').pointer({
			content: '<?php echo $content; ?>',
			position: {
				edge: '<?php echo $this->edge; ?>',
				align: '<?php echo $this->align; ?>'
			},
			close: function() {
				$.post(ajaxurl, {
					pointer: '<?php echo $this->name; ?>',
					action: 'dismiss-wp-pointer'
				});
			}
		}).pointer('open');
	});
</script>
<?php
	}

	public static function dismissed_pointers() {
		return explode( ',', (string) get_user_meta( get_current_user_id(),
			'dismissed_wp_pointers', true ) );
	}
}
?>
