<?php
/*
Plugin Name: WP Extra Comment Fields
Plugin URI: http://www.solaceten.info
Description: Add extra comment form fields (dropdowns, radios, text) to the standard WordPress comment form and make it more useful. Free Full Version.
Version: 2.24
Author: Solace and Tom Wright
Author URI: http://www.solaceten.info
*/

define( ECF_VERSION, '2.24' );

function ecf_plugin_data( $name = null ) {
	$data = array(
		
		'version' => ECF_VERSION,
		'website' => 'http://www.solaceten.info/',
		'edition' => 'Free full version',
		'authors' => array( 'Copyright 2013 Solace', 'T Wright' ),
		'links' => array(
			'Support' => 'http://www.solaceten.info/')
			
	);
	return is_null( $name ) ? $data : $data[$name];
}

function ecf_js_dump( $msg ) {
	echo '<script type="text/javascript">alert(';
	echo json_encode( var_export( $msg, true ) );
	echo ');</script>';
}

function ecf_file_dump( $msg ) {
	$filename = 'msg.txt';
	$f = fopen( $filename, 'a' );
	fwrite( $f, var_export( $msg, true ) . "\n" );
	fclose( $f );
}

require_once 'interface-ecf-unavailable.php';
require_once 'class-ecf-field-type.php';
require_once 'class-ecf-option.php';
require_once 'class-ecf-option-integer.php';
require_once 'class-ecf-option-boolean.php';
require_once 'class-ecf-option-list.php';
require_once 'interface-ecf-visibility-condition.php';

// Include all custom field types
foreach ( glob( dirname( __FILE__ ) . '/field-types/*.php' )
	as $filename ) {
	include_once $filename;
}
// Include all options
foreach ( glob( dirname( __FILE__ ) . '/options/*.php' )
	as $filename ) {
	include_once $filename;
}

require_once 'class-ecf-settings-global.php';
$GLOBALS['ecf_settings_global'] = new ECF_Settings_Global();

require_once 'class-ecf-db.php';
$GLOBALS['ecfdb'] = new ECF_DB();
require_once 'class-ecf-main.php';
new ECF_Main();
