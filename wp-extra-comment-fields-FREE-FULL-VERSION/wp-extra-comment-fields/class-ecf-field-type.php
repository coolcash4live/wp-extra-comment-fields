<?php
/* An abstract class for providing form types */
abstract class ECF_Field_Type {
	private static $types = array();
	protected $name;

	/* Constructor */
	public function __construct() {
		self::register_type( $this->name, $this );
	}

	/* Add a type to the types list */
	private static function register_type( $name, $type ) {
		self::$types[$name] = $type;
	}

	/* Return the appropriate type */
	public static function get_type( $name ) {
		return array_key_exists( $name, self::$types )
			? self::$types[$name] : self::get_default_type();
	}

	/* Return all types */
	public static function get_types() {
		return self::$types;
	}

	/* Get default type */
	public static function get_default_type() {
		return self::get_type( 'text' );
	}

	/* Display form field */
	public abstract function form_field( $name, $field );
	
	/* Display the field's content */
	public function display_field( $id, $name, $value ) {
		return "<span class='ecf-field ecf-field-$id'>"
			. "<strong class='ecf-question'>$name:</strong>"
			. " <span class='ecf-answer'>$value</span></span>\n";
	}

	/* Display field plain text suitable for email display */
	public function display_plaintext_field( $name, $value ) {
		return "$name: $value";
	}

	/* Get the description */
	abstract public function get_description();
}
?>
