<?php
class ECF_Option_Public extends ECF_Option_Boolean {
	
	public function get_name() {
		return 'public';
	}

	public function get_full_name() {
		return 'Public';
	}

	public function get_description() {
		return 'Enable this for the field to be displayed in the '
			. 'comments section of posts.';
	}

	public function get_default_value() {
		return true;
	}

	public function priority() {
		return 80;
	}
}
new ECF_Option_Public();
?>