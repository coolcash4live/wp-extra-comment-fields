<?php
class ECF_Option_Order extends ECF_Option_Integer {
	
	public function get_name() {
		return 'order';
	}

	public function get_full_name() {
		return 'Order';
	}

	public function get_description() {
		return 'The order which determines where this field will be displayed '
			. 'within the comment form. Fields with highter orders will be '
			. 'displayed nearer the top of the form.';
	}

	public function get_default_value() {
		return 0;
	}

	public function priority() {
		return 90;
	}
}
new ECF_Option_Order();
?>