<?php
class ECF_Option_Post_Types extends ECF_Option_List
	implements ECF_Visibility_Condition {
	
	public function get_name() {
		return 'post_types';
	}

	public function get_full_name() {
		return 'Post Types';
	}

	public function get_description() {
		return 'The custom post types for which this field is to be '
			. 'displayed.';
	}

	public function get_default_value() {
		return '';
	}

	public function is_satisfied( $field ) {
		global $post;

		$post_types = $this->get_value( $field );
		// The post type must be in the list of valid post types
		return '' == $post_types[0]
			|| in_array( get_post_type(), $post_types );
	}

	public function priority() {
		return 54;
	}
}
new ECF_Option_Post_Types();
?>