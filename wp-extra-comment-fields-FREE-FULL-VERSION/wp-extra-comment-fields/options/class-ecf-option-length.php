<?php
class ECF_Option_Length extends ECF_Option_Integer {
	
	public function __construct() {
		parent::__construct( array( 'textarea' ) );
	}

	public function get_name() {
		return 'length';
	}

	public function get_full_name() {
		return 'Length';
	}

	public function get_description() {
		return 'The length of the text area (number of rows).';
	}

	public function get_default_value() {
		return 5;
	}

	public function priority() {
		return 95;
	}
}
new ECF_Option_Length();
?>